=== Avventura Lite ===
Contributors: alexvtn
Tags: custom-background, custom-colors, custom-logo, custom-menu, featured-images, footer-widgets, post-formats, right-sidebar, sticky-post, theme-options, threaded-comments, translation-ready, one-column, two-columns, three-columns, grid-layout, blog, e-commerce, photography
Requires at least: 4.0
Tested up to: 5.1.1
Stable tag: 1.0.5
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Avventura Lite is an elegant and beauty WordPress theme, perfect for writers, bloggers and photographers.

== Description ==

Avventura Lite is an elegant and beauty WordPress theme, perfect for writers, bloggers and photographers. Avventura Lite is also fully compatible with Contact Form 7 and WooCommerce plugin and is optimized for mobile devices.

Created by ThemeinProgress, http://www.themeinprogress.com
Avventura Lite WordPress Theme, Copyright 2019 ThemeinProgress.
Avventura Lite is distributed under the terms of the GPLv2 or later

== Changelog ==

= 1.0.5 =
Release date - April, 09 - 2019

* Tested with WordPress 5.1.1
* Fixed - WooCommerce alerts
* Edit - Code optimized

= 1.0.4 =
Release date - March, 11 - 2019

* Updated - Readme.txt file
* Edit - Code optimized

= 1.0.3 =
Release date - March, 01 - 2019

* Tested with WordPress 5.1
* Edit - Code optimized

= 1.0.2 =
Release date - February, 09 - 2019

* Edit - Code optimized

= 1.0.1 =
* Initial version

== Resources ==

Screenshot images By pxhere - https://www.pxhere.com
* Featured image : https://pxhere.com/en/photo/1458369/ by Vikysha ( https://pxhere.com/en/photographer/1757421/ ) - CC0
* Featured image : https://pxhere.com/en/photo/1543007/ by photo716 ( https://pxhere.com/en/photographer/1887041/ ) - CC0
* Featured image : https://pxhere.com/en/photo/1566111/ by Ken Lane ( https://pxhere.com/en/photographer/946697/ ) - CC0
* Featured image : https://pxhere.com/en/photo/1436001/ by rawpixel.com ( https://pxhere.com/en/photographer/795663/ ) - CC0
* Featured image : https://pxhere.com/en/photo/1569027/ by Peter Fischer ( https://pxhere.com/en/photographer/2003765/ ) - CC0

Font Awesome By Dave Gandy - http://fortawesome.github.io/Font-Awesome/
** Font License under SIL OFL 1.1 - http://scripts.sil.org/OFL ( Applies to all desktop and webfont files in the following directory: /avventura-lite/assets/fonts/ )
** Code License under MIT License - http://opensource.org/licenses/mit-license.html ( Applies to the font-awesome.css file in /avventura-lite/assets/css/ )
** Brand Icons - All brand icons are trademarks of their respective owners. The use of these trademarks does not indicate endorsement of the trademark holder by Font Awesome, nor vice versa.

Google Web Fonts (Cinzel Decorative, Merriweather, Playfair Display) By Google - http://google.com
** Cinzel Decorative, Font by Natanael Gama, Licensed under Open Font License, https://fonts.google.com/specimen/Cinzel+Decorative
** Merriweather, Font by Sorkin Type, Licensed under Open Font License, https://fonts.google.com/specimen/Merriweather
** Playfair Display, Font by Claus Eggers Sørensen, Licensed under Open Font License, https://fonts.google.com/specimen/Playfair+Display

Bootstrap By Twitter, Inc - http://getbootstrap.com
* bootstrap.css v3.3.7 - Licensed under MIT license ( Applies to bootstrap.css file in /avventura-lite/assets/css/ )

prettyPhoto By Stephane Caron - http://www.no-margin-for-errors.com/projects/prettyphoto-jquery-lightbox-clone/ 
* prettyPhoto.css v3.1.6 - Licensed under GPLv2 or Creative Commons 2.5 license ( Applies to prettyPhoto.css file in /avventura-lite/assets/css/ )

Slick Slider By Ken Wheeler - https://github.com/kenwheeler/slick/
* slick.css - Licensed under MIT license ( Applies to slick.css file in /avventura-lite/assets/css/ )

jQuery Easing By George McGinley Smith - http://gsgd.co.uk/sandbox/jquery/easing/
* jquery.easing.js v1.3 - Licensed under BSD License ( Applies to jquery.easing.js file in /avventura-lite/assets/js/ )

jQuery Nicescroll By InuYaksa - https://nicescroll.areaaperta.com/
* jquery.nicescroll.js v3.7.6 - Licensed under MIT license ( Applies to jquery.nicescroll.js file in /avventura-lite/assets/js/ )

jQuery TouchSwipe By Matt Bryson - https://github.com/mattbryson/TouchSwipe-Jquery-Plugin
* jquery.touchSwipe.js v1.6.18 - Licensed under MIT license ( Applies to jquery.touchSwipe.js file in /avventura-lite/assets/js/ )

prettyPhoto By Stephane Caron - http://www.no-margin-for-errors.com
* prettyPhoto.js v3.1.4 - Licensed under MIT license ( Applies to prettyPhoto.js file in /avventura-lite/assets/js/ )

Slick Slider By Ken Wheeler - https://github.com/kenwheeler/slick/
* slick.js v3.1.4 - Licensed under MIT license ( Applies to slick.js file in /avventura-lite/assets/js/ )

HTML5 Shiv By @afarkas @jdalton @jon_neal @rem - https://github.com/aFarkas/html5shiv
* html5shiv.js v3.7.0 - Licensed under MIT and GPL2 license ( Applies to html5shiv.js file in /avventura-lite/assets/scripts/ )

selectivizr By Keith Clark - https://github.com/keithclark/selectivizr
* selectivizr.js v1.0.3b - Licensed under MIT license ( Applies to selectivizr.js file in /avventura-lite/assets/scripts/ )

The following scripts and styles are coded by me and they are released under MIT license

* /avventura-lite/assets/skins/cyan.css
* /avventura-lite/assets/skins/orange.css
* /avventura-lite/assets/skins/blue.css
* /avventura-lite/assets/skins/red.css
* /avventura-lite/assets/skins/pink.css
* /avventura-lite/assets/skins/purple.css
* /avventura-lite/assets/skins/yellow.css
* /avventura-lite/assets/skins/green.css
* /avventura-lite/assets/skins/black.css
* /avventura-lite/assets/css/avventura-lite-woocommerce.css
* /avventura-lite/assets/css/avventura-lite-template.css
* /avventura-lite/assets/js/avventura-lite-template.js
* /avventura-lite/core/admin/assets/css/customize.css
* /avventura-lite/core/admin/assets/css/notice.css
* /avventura-lite/core/admin/assets/css/panel.css
* /avventura-lite/core/admin/assets/js/panel.js

The following images are created by me and they are released under CC0 license

* /avventura-lite/assets/images/background/paper.jpg
* /avventura-lite/assets/images/icons/scotch-left.png
* /avventura-lite/assets/images/icons/scotch-right.png
* /avventura-lite/assets/images/placeholder-400x400.jpg
* /avventura-lite/assets/images/placeholder-940x600.jpg